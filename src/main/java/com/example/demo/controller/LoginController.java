package com.example.demo.controller;

import com.example.demo.lib.base.table.Manager;
import com.example.demo.lib.base.table.ManagerRepository;
import com.example.demo.lib.jwt.ManagerApiTokenUtil;
import com.example.demo.model.LoginParam;
import com.example.demo.model.ResponseBase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Tag(name = "登入驗證")
@RestController
@RequestMapping
public class LoginController {

    @Autowired
    ManagerRepository managerRepository;

    @PostMapping(value = "/login")
    @Operation(summary = "以帳號密碼登入取得Token")
    @SecurityRequirements
    public ResponseBase<String> login(@RequestBody @Parameter(description = "登入資訊") LoginParam loginParam, HttpServletRequest request) throws Exception {
        System.out.println(loginParam.getAccount());
        System.out.println(loginParam.getPassword());
        List<Manager> managerList = managerRepository.findByAccount(loginParam.getAccount());
        if (!managerList.isEmpty()) {
            String password = managerList.get(0).getPassword();
            if (password.equals(loginParam.getPassword())) {
                String token = ManagerApiTokenUtil.generateToken(managerList.get(0).getId(), managerList.get(0).getName(), managerList.get(0).getPermission());
                return new ResponseBase<>(token);
            }
            throw new Exception("wrong password");
        }
        throw new Exception("unknown user");
    }

}
