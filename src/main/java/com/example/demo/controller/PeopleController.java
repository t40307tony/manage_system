package com.example.demo.controller;

import com.example.demo.lib.base.table.People;
import com.example.demo.lib.base.table.PeopleRepository;
import com.example.demo.lib.jwt.ManagerApiTokenUtil;
import com.example.demo.model.ResponseBase;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.util.List;


@Tag(name = "取得民眾資料")
@RestController
@RequestMapping("/people")
public class PeopleController {

    @Autowired
    PeopleRepository peopleRepository;

    @Autowired
    DataSource dataSource;

    @GetMapping
    public ResponseBase<List<People>> getPeople(HttpServletRequest request) throws Exception {
        ManagerApiTokenUtil.getManagerInfo(request).verifyPermission(request.getRequestURI(), request.getMethod());
        System.out.println(request.getMethod()); //GET/POST
        System.out.println(request.getRequestURI()); // /Temple/people
        List<People> peopleList = peopleRepository.findAll();
        peopleList.forEach(people -> {
            people.setAccount(null);
            people.setPassword(null);
        });
        return new ResponseBase<>(peopleList);
    }

    @GetMapping("/{name}")
    public ResponseBase<People> getPeopleByName(HttpServletRequest request, @PathVariable String name) throws Exception {
        ManagerApiTokenUtil.getManagerInfo(request).verifyPermission(request.getRequestURI(), request.getMethod());
        List<People> peopleList = peopleRepository.findByName(name);
        peopleList.forEach(people -> {
            people.setAccount(null);
            people.setPassword(null);
        });
        return peopleList.isEmpty() ? new ResponseBase<>() : new ResponseBase<>(peopleList.get(0));
    }

    @GetMapping("/group/{name}")
    public ResponseBase<List<People>> getPeopleByGroupName(HttpServletRequest request, @PathVariable String name) throws Exception {
        ManagerApiTokenUtil.getManagerInfo(request).verifyPermission(request.getRequestURI(), request.getMethod());
        List<People> peopleList = peopleRepository.findByFamily_Name(name);
        peopleList.forEach(people -> {
            people.setAccount(null);
            people.setPassword(null);
        });
        return peopleList.isEmpty() ? new ResponseBase<>() : new ResponseBase<>(peopleList);
    }

    @PostMapping
    public ResponseBase<People> addPeople(HttpServletRequest request, @RequestBody People people) throws Exception {
        ManagerApiTokenUtil.getManagerInfo(request).verifyPermission(request.getRequestURI(), request.getMethod());
        People newPeople = peopleRepository.saveAndFlush(people);
        return new ResponseBase<>(newPeople);
    }
}
