package com.example.demo.controller;

import com.example.demo.lib.base.table.Family;
import com.example.demo.lib.base.table.FamilyRepository;
import com.example.demo.lib.base.table.People;
import com.example.demo.lib.base.table.PeopleRepository;
import com.example.demo.lib.jwt.ManagerApiTokenUtil;
import com.example.demo.model.ResponseBase;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.util.List;
import java.util.function.Consumer;

@Tag(name = "取得民眾資料")
@RestController
@RequestMapping("/family")
public class FamilyController {
    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    DataSource dataSource;

    @GetMapping()
    public ResponseBase<List<Family>> getFamily(HttpServletRequest request) throws Exception {
        ManagerApiTokenUtil.getManagerInfo(request).verifyPermission(request.getRequestURI(), request.getMethod());
        List<Family> familyList = familyRepository.findAll();
        return new ResponseBase<>(familyList);
    }

    @GetMapping("/{name}")
    public ResponseBase<Family> getFamilyByName(HttpServletRequest request, @PathVariable String name) throws Exception {
        ManagerApiTokenUtil.getManagerInfo(request).verifyPermission(request.getRequestURI(), request.getMethod());
        List<Family> familyList = familyRepository.findByName(name);
        return familyList.isEmpty() ? null : new ResponseBase<>(familyList.get(0));
    }

    @PostMapping
    public ResponseBase<Family> addFamily(HttpServletRequest request, @RequestBody Family family) throws Exception {
        ManagerApiTokenUtil.getManagerInfo(request).verifyPermission(request.getRequestURI(), request.getMethod());
        if (!familyRepository.findByName(family.getName()).isEmpty())
            throw new Exception("family name is duplicated.");
        try {
            Family newPeople = familyRepository.saveAndFlush(family);
            return new ResponseBase<>(newPeople);
        } catch (Exception e) {
            throw new Exception("add family fail.");
        }
    }
}
