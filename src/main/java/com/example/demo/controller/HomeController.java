package com.example.demo.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Tag(name = "首頁/測試")
@RestController
@RequestMapping
public class HomeController {

    @GetMapping
    @Operation(summary = "首頁")
    public void welcome(HttpServletRequest request){
        System.out.println("test home");

    }

    @GetMapping("/test")
    @Operation(summary = "test")
    public String test(@RequestParam(value = "Msg", required = false, defaultValue = "") String data, HttpServletRequest request){
        System.out.println("data is " + data);
        return data;
    }

}
