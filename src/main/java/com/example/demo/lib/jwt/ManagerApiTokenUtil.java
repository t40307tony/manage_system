package com.example.demo.lib.jwt;

import com.example.demo.model.Keywords;
import com.example.demo.model.ResponseBaseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ManagerApiTokenUtil {

    private static final String KEY = "testTonyWeb_sad5dsa1f23weqr2ssw";
    private static final String HEADER_STRING = "Authorization";
    private static final long EXPIRATION_TIME = 86_400_000;
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String getHeaderString() {
        return HEADER_STRING;
    }

    public static String generateToken(int id, String name, int permission) {
        Map<String, Object> customerInfo = new HashMap<>();
        customerInfo.put(Keywords.id, id);
        customerInfo.put(Keywords.name, name);
        customerInfo.put(Keywords.permission, permission);

        return Jwts.builder().setClaims(customerInfo)
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
            .signWith(SignatureAlgorithm.HS512, KEY).compact();
    }

    public static ResponseBaseStatus verifyToken(String token) {
        if (token == null || token.equals("")) {
            return ResponseBaseStatus.TOKEN_EMPTY;
        }

        try {
            Jwts.parser().setSigningKey(KEY).parseClaimsJws(token).getBody();
            return ResponseBaseStatus.SUCCESS;
        } catch (SignatureException | MalformedJwtException e) {
            return ResponseBaseStatus.TOKEN_INVALID;
        } catch (ExpiredJwtException e) {
            return ResponseBaseStatus.TOKEN_EXPIRED;
        } catch (JwtException e) {
            return ResponseBaseStatus.ERROR;
        }
    }

    public static String getToken(HttpServletRequest request) {
        return Objects.toString(request.getHeader(getHeaderString()), "").replace("Bearer", "");
    }

    public static ManagerInfo getManagerInfo(HttpServletRequest request) {
        String token = getToken(request);
        return getManagerInfo(token);
    }

    public static ManagerInfo getManagerInfo(String token) {
        if (token == null) {
            return null;
        }

        try {
            Claims claims = Jwts.parser().setSigningKey(KEY).parseClaimsJws(token).getBody();
            int id = (Integer)claims.get(Keywords.id);
            String name = (String)claims.get(Keywords.name);
            int permission = (Integer)claims.get(Keywords.permission);
            return new ManagerInfo(id, name, permission);
        } catch (JwtException e) {
            return null;
        }
    }
}
