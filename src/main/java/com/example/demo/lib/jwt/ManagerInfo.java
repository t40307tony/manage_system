package com.example.demo.lib.jwt;


public class ManagerInfo {

    private int id;
    private String name;
    private int permission;

    public ManagerInfo() {

    }

    public ManagerInfo(int id, String name, int permission) {
        this.id = id;
        this.name = name;
        this.permission = permission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    public void verifyPermission(String path, String requestMethod) throws Exception {
        if (permission < 1) {
            return;
        } else if (requestMethod.equals("POST")) {
            throw new Exception("permission denied");
        } else if (permission >= 1 && path.contains("family")) {
            throw new Exception("permission denied");
        } else if (permission >= 5 && path.contains("people")) {
            throw new Exception("permission denied");
        }
    }
}
