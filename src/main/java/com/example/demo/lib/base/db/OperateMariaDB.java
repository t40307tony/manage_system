package com.example.demo.lib.base.db;

import org.apache.commons.dbcp2.BasicDataSource;

public abstract class OperateMariaDB {

    private static final Object object = new Object();
    private static OperateMariaDB mariadb = null;

    private BasicDataSource datasource = null;
    private final static String DRIVER = "org.mariadb.jdbc.Driver";
    private String ip, port, username, password;
    private int maxIdlePool = -1, minIdelPool = 50, maxTotalPool = 100;


    private Boolean isInitializing = false;
    protected abstract String getDbName();


    public OperateMariaDB() throws ClassNotFoundException {
        Class.forName(DRIVER);
//        mariadb =
    }

    private void InitPool() throws Exception {
        if (datasource == null) {
            synchronized (object) {
                try {
                    if (datasource == null) {
                        isInitializing = true;
                        datasource = new BasicDataSource();
                        String connectString = GetConnectString();
                        // PoolProperties p = new PoolProperties();
                        // p.setUrl(connectString);
                        // p.setDriverClassName(ServerType);
                        // p.setMaxActive(20);
                        // p.setMinIdle(2);
                        // p.setMaxIdle(5);
                        // datasource.setPoolProperties(p);
                        datasource.setDriverClassName(DRIVER);
                        datasource.setMaxIdle(maxIdlePool); // Pool中最大的空閒的連線數量，也就是所能容納狀態為idle的DB Connection的最大數量，-1 表示無上限
                        datasource.setMinIdle(minIdelPool); // Pool中最小的空閒的連線數量
                        datasource.setMaxTotal(maxTotalPool); // Pool在同一時刻內所提供的最大活動連接數
                        datasource.setUrl(connectString);
                        System.out.println("InitPool：" + connectString);
                        isInitializing = false;
                    }
                } catch (Exception e) {
                    datasource = null;
                    isInitializing = false;
                    throw e;
                }
            }
        }
    }

    protected String GetConnectString() throws Exception {
        return "jdbc:mysql://" + ip + ":" + port
                + "/" + getDbName()
                + "?user=" + username
                + "&password=" + password
                + "&autoReconnect=true&useSSL=false";
    }

}
