package com.example.demo.lib.base.table;

import javax.persistence.*;
@Entity
@Table(name = "manager")
public class Manager {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @Column(name="account", nullable = false)
    private String account;
    @Column(name="password", nullable = false)
    private String password;
    @Column(name="name")
    private String name;
    @Column(name="permission")
    private short permission;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getPermission() {
        return permission;
    }

    public void setPermission(short permission) {
        this.permission = permission;
    }
}
