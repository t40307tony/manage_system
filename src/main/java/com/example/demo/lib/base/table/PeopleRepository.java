package com.example.demo.lib.base.table;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PeopleRepository extends JpaRepository<People, Integer> {
    List<People> findAll();

    List<People> findByName(String name);

    List<People> findByFamily_Name(String name);
}
