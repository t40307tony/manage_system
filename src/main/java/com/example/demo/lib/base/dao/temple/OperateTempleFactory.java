package com.example.demo.lib.base.dao.temple;

public class OperateTempleFactory {

    private static final Object object = new Object();
    private static OperateTempleFactory templeDb = null;
    private final OperateTablePeople people;

    private OperateTempleFactory(){
        people= new OperateTablePeople();
    }

    public static OperateTempleFactory getInstance(){
        if(templeDb==null){
            synchronized (templeDb){
                if(templeDb==null){
                    templeDb = new OperateTempleFactory();
                }
            }
        }
        return templeDb;
    }

    public OperateTablePeople getPeople() {
        return people;
    }
}
