package com.example.demo.lib.base.table;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "family")
public class Family {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @Column(name="name", nullable=false, unique = true)
    private String name;

    @OneToMany(mappedBy = "family")
    private Set<People> peopleSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
