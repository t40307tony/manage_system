package com.example.demo.lib.base.table;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FamilyRepository extends JpaRepository<Family, Integer> {
    List<Family> findAll();

    List<Family> findByName(String name);
}
