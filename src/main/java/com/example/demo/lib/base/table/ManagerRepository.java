package com.example.demo.lib.base.table;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
    List<Manager> findAll();

    List<Manager> findByAccount(String account);
}
