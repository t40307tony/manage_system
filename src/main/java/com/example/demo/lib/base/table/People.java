package com.example.demo.lib.base.table;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "people", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "birth", "address"})})
public class People {

    @Schema(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name", nullable = false)
    @Schema(description = "姓名")
    private String name;
    @Column(name = "birth", nullable = false)
    @Schema(description = "生日(包含時辰)")
    private Date birth;
    @Column(name = "address")
    @Schema(description = "地址")
    private String address;
    @Column(name = "account")
    @Schema(hidden = true)
    private String account;
    @Column(name = "password")
    @Schema(hidden = true)
    private String password;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Family family;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
}
