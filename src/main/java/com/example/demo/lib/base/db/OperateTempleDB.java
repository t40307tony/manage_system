package com.example.demo.lib.base.db;

public class OperateTempleDB extends OperateMariaDB{

    private static final String dbBName = "Temple";
    private static final Object sync = new Object();
    private static OperateTempleDB templeDB = null;

    public OperateTempleDB() throws ClassNotFoundException {
        super();
    }

    @Override
    protected String getDbName() {
        return dbBName;
    }

    public static OperateTempleDB getInstance() throws ClassNotFoundException {
        if(templeDB==null){
            synchronized (sync){
                if(templeDB==null){
                    templeDB = new OperateTempleDB();
                }
            }
        }
        return templeDB;
    }
}
