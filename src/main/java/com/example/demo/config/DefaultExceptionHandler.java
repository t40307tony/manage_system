package com.example.demo.config;

import com.example.demo.model.ResponseBase;
import com.example.demo.model.ResponseBaseStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class DefaultExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ResponseBase<String>> unKnowException(Exception e) {
        ResponseBase<String> responseBase = new ResponseBase<>();
        responseBase.setResponseStatus(ResponseBaseStatus.ERROR);
        responseBase.setResponseMsg(e.getMessage());
        return new ResponseEntity<>(responseBase, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<ResponseBase<String>> httpMessageNotReadableException(Exception e) {
        ResponseBase<String> responseBase = new ResponseBase<>();
        responseBase.setResponseStatus(ResponseBaseStatus.ERROR);
        responseBase.setResponseMsg(e.getMessage());
        return new ResponseEntity<>(responseBase, HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ResponseBase<String>> noHandlerFoundException(HttpServletRequest request, Exception e) {
        ResponseBase<String> responseBase = new ResponseBase<>();
        responseBase.setResponseStatus(ResponseBaseStatus.ERROR_PATH);
        responseBase.setResponseMsg(request.getServletPath());
        return new ResponseEntity<>(responseBase, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<ResponseBase<String>> httpRequestMethodNotSupportedException(Exception e) {
        ResponseBase<String> responseBase = new ResponseBase<>();
        responseBase.setResponseStatus(ResponseBaseStatus.ERROR);
        responseBase.setResponseMsg(e.getMessage());
        return new ResponseEntity<>(responseBase, HttpStatus.METHOD_NOT_ALLOWED);
    }
}
