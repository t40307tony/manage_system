package com.example.demo.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.util.*;

@Component
@Aspect
public class LogAspectConfig {

    private static final List<Integer> accountList = new ArrayList<>();
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Pointcut("execution(public * com.eup.fms.soap.*.controller..*.*(..)) && @within(org.springframework.web.bind.annotation.RestController)")
    public void pointcut() {

    }

    public static void start(Integer vendorId) {
        accountList.add(vendorId);
    }

    public static void end(Integer vendorId) {
        accountList.remove(vendorId);
    }

    public static boolean isAlreadyCall(Integer vendorId) {
        return accountList.contains(vendorId);
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed(); // 呼叫此方法會繼續執行後續方法
        long executeTime = System.currentTimeMillis() - startTime;
        if (executeTime >= 10 * 1000) {
            Map<String, Object> fieldMap = getParams(joinPoint);
            fieldMap.put("execution_time", (int) ((executeTime - startTime) / 100) / 10.0 + "s");
            System.out.println(fieldMap + ", 命令時間超過10秒");
        }
        return result;
    }

    @AfterThrowing(value = "pointcut()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        try {
            System.out.println(getParams(joinPoint));
            e.printStackTrace();
        } catch (Exception ignored) {
        }
    }

    private Map<String, Object> getParams(JoinPoint joinPoint) throws JsonProcessingException {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
            .getRequest();
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Annotation[][] annotationMatrix = methodSignature.getMethod().getParameterAnnotations();

        StringBuilder requestParam = new StringBuilder();
        String requestBody = "";
        for (int i = 0; i < annotationMatrix.length; i++) {
            for (Annotation annotation : annotationMatrix[i]) {
                if (annotation instanceof RequestParam) {
                    requestParam.append(methodSignature.getParameterNames()[i])
                        .append("=")
                        .append(joinPoint.getArgs()[i]);
                } else if (annotation instanceof RequestBody) {
                    requestBody = objectMapper.writeValueAsString(joinPoint.getArgs()[i]);
                }
            }
        }

        String param = requestParam.toString().equals("") ? "" : "?" + requestParam;
        Map<String, Object> fieldMap = new HashMap<>();
        fieldMap.put("uri", request.getRequestURL() + param);
        fieldMap.put("ip", getIpAddress(request));
        fieldMap.put("param", requestBody);
        return fieldMap;
    }

    private String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
        } else if (ip.length() > 15) {
            String[] ips = ip.split(",");
            for (String s : ips) {
                if (!("unknown".equalsIgnoreCase(s))) {
                    ip = s;
                    break;
                }
            }
        }
        return ip;
    }
}
