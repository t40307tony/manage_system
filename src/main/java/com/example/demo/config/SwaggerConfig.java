package com.example.demo.config;

import com.example.demo.lib.jwt.ManagerApiTokenUtil;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI openApiInfo() {
        return new OpenAPI()
                .info(new Info().title("Eup Message & Verification Restful API")
                        .title("Eup Vendor API")
                        .description("Eup API Platform")
                        .version("V1.0.1.1")
                        .contact(new Contact().name("Eup").url("https://www.eup.tw")))
                .components(new Components()
                        .addSecuritySchemes(ManagerApiTokenUtil.getHeaderString(),
                                new SecurityScheme().name(ManagerApiTokenUtil.getHeaderString())
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .bearerFormat("JWT")
                                        .in(SecurityScheme.In.HEADER)))
                .addSecurityItem(new SecurityRequirement().addList(ManagerApiTokenUtil.getHeaderString(), Arrays.asList("read", "write")));
    }

    @Bean
    public OperationCustomizer customizeOperation() {
        return (operation, handlerMethod) ->
                operation.responses(operation.getResponses()
                        .addApiResponse("400", new ApiResponse().description("Bad Request"))
                        .addApiResponse("401", new ApiResponse().description("Token Unauthenticated"))
                        .addApiResponse("403", new ApiResponse().description("Token Unauthorized"))
                        .addApiResponse("404", new ApiResponse().description("Error Path"))
                        .addApiResponse("405", new ApiResponse().description("Method Not Allowed"))
                        .addApiResponse("500", new ApiResponse().description("Internal Server Error")));
    }

    @Bean
    public GroupedOpenApi allApi() {
        return GroupedOpenApi.builder()
                .group("All")
                .pathsToMatch("/**")
                .addOperationCustomizer(customizeOperation())
                .build();
    }
}