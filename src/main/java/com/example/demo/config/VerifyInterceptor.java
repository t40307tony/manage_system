package com.example.demo.config;

import com.example.demo.lib.jwt.ManagerApiTokenUtil;
import com.example.demo.lib.jwt.ManagerInfo;
import com.example.demo.model.ResponseBase;
import com.example.demo.model.ResponseBaseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class VerifyInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        String token = ManagerApiTokenUtil.getToken(request);
        ResponseBaseStatus verifyStatus = ManagerApiTokenUtil.verifyToken(token);
        ResponseBase<String> responseBase = new ResponseBase<>();
        switch (verifyStatus) {
            case TOKEN_EMPTY:
                responseBase.setResponseStatus(ResponseBaseStatus.TOKEN_EMPTY);
                responseBase.setResponseMsg(ResponseBaseStatus.TOKEN_EMPTY.name());
                response.getWriter().write(new ObjectMapper().writeValueAsString(responseBase));
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            case TOKEN_EXPIRED:
                responseBase.setResponseStatus(ResponseBaseStatus.TOKEN_EXPIRED);
                responseBase.setResponseMsg(ResponseBaseStatus.TOKEN_EXPIRED.name());
                response.getWriter().write(new ObjectMapper().writeValueAsString(responseBase));
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            case TOKEN_INVALID:
                responseBase.setResponseStatus(ResponseBaseStatus.TOKEN_INVALID);
                responseBase.setResponseMsg(ResponseBaseStatus.TOKEN_INVALID.name());
                response.getWriter().write(new ObjectMapper().writeValueAsString(responseBase));
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            case SUCCESS:
                ManagerInfo info = ManagerApiTokenUtil.getManagerInfo(token);
                LogAspectConfig.start(info.getId());
                return true;
            default:
                responseBase.setResponseStatus(ResponseBaseStatus.ERROR);
                responseBase.setResponseMsg(ResponseBaseStatus.ERROR.name());
                response.getWriter().write(new ObjectMapper().writeValueAsString(responseBase));
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        String token = ManagerApiTokenUtil.getToken(request);
        LogAspectConfig.end(ManagerApiTokenUtil.getManagerInfo(token).getId());
    }
}
