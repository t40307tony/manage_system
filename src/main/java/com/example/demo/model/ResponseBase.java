package com.example.demo.model;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Collection;

public class ResponseBase<T> {

    @Schema(description = "此次呼叫狀態", example = "200")
    private ResponseBaseStatus responseStatus = ResponseBaseStatus.SUCCESS;
    @Schema(description = "訊息", example = "Success")
    private String responseMsg = ResponseBaseStatus.SUCCESS.toString();
    @Schema(description = "查詢結果、異動成功結果")
    private T result;
    @Schema(description = "更新失敗結果(僅在新增/修改/刪除時有用)")
    private T failResult;

    public ResponseBase() {

    }

    public ResponseBase(T result) {
        this.result = result;
        if (result == null || (result instanceof Collection && ((Collection<?>) result).size() == 0)) {
            responseStatus = ResponseBaseStatus.DATA_EMPTY;
        }
    }

    public int getResponseStatus() {
        return responseStatus.getValue();
    }

    public void setResponseStatus(ResponseBaseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public T getFailResult() {
        return failResult;
    }

    public void setFailResult(T failResult) {
        this.failResult = failResult;
    }
}
