package com.example.demo.model;

public enum ResponseBaseStatus {
    // 100: 正常
    SUCCESS(100),
    DATA_EMPTY(101),
    // 200: 各類異常
    ERROR(200),
    ERROR_PATH(201),
    ID_NOT_MATCH(202),
    OUT_OF_LIMIT(203),
    // 300: TOKEN相關
    TOKEN_EMPTY(300),
    TOKEN_EXPIRED(301),
    TOKEN_INVALID(302),
    // 900: 其他
    ACCOUNT_EXIST(900);

    private final int value;

    ResponseBaseStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
