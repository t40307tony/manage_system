package com.example.demo.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class LoginParam {

    @Schema(description = "帳號")
    private String account;
    @Schema(description = "密碼")
    private String password;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
